package com.aeq.invoice;

import com.aeq.invoice.dao.CustomerDao;
import com.aeq.invoice.dao.InvoiceDao;
import com.aeq.invoice.dao.OrderDao;
import com.aeq.invoice.dao.ProductDao;
import com.aeq.invoice.dao.impl.CustomerDaoImpl;
import com.aeq.invoice.dao.impl.InvoiceDaoImpl;
import com.aeq.invoice.dao.impl.OrderDaoImpl;
import com.aeq.invoice.dao.impl.ProductDaoImpl;
import com.aeq.invoice.domain.Customer;
import com.aeq.invoice.domain.Order;
import com.aeq.invoice.domain.Product;
import com.aeq.invoice.service.CustomerService;
import com.aeq.invoice.service.InvoiceService;
import com.aeq.invoice.service.OrderService;
import com.aeq.invoice.service.ProductService;
import com.aeq.invoice.service.impl.CustomerServiceImpl;
import com.aeq.invoice.service.impl.InvoiceServiceImpl;
import com.aeq.invoice.service.impl.OrderServiceImpl;
import com.aeq.invoice.service.impl.ProductServiceImpl;

import java.util.Optional;

public class Main {

    public static void main(String[] args) {

        CustomerDao customerDao = new CustomerDaoImpl();
        CustomerService customerService = new CustomerServiceImpl();

        ProductDao productDao = new ProductDaoImpl();
        ProductService productService = new ProductServiceImpl();

        InvoiceDao invoiceDao = new InvoiceDaoImpl();
        InvoiceService invoiceService = new InvoiceServiceImpl();


        OrderDao orderDao = new OrderDaoImpl();
        OrderService orderService = new OrderServiceImpl();

        customerService.addCustomer(customerDao);
        customerService.addCustomer(customerDao);

        Optional<Customer> customer = customerService.findCustomerByid(customerDao, 2l);

        if (customer.isPresent()) {
            System.out.println("Customer found by id");
            System.out.println(customer.get());
        } else {
            System.out.println("Customer not found by given id");
        }

        System.out.println("add product details");
        productService.addProduct(productDao);
        productService.addProduct(productDao);
        productService.addProduct(productDao);
        productService.addProduct(productDao);


        System.out.println("Make and Order");

        Optional<Product> product = productService.findProductById(productDao, 1l);

        if (product.isPresent()) {
            System.out.println("product found by id");
            System.out.println(product.get());
        } else {
            System.out.println("product not found by given id");
        }


        orderService.addOrder(customerDao, orderDao, invoiceDao, productDao);
        Optional<Order> order = orderService.findByOrderId(orderDao, 1l);

        if (order.isPresent()) {
            System.out.println("order found by id");
            System.out.println(order.get());
        } else {
            System.out.println("order not found by given id");
        }

        System.out.println("generating invoice");

        invoiceService.addInvoice(customerDao, orderDao, invoiceDao, productDao, order.get().getCustomerid(), order.get().getId());

        System.out.println("Invoices");
        System.out.println(invoiceDao.list());

    }
}
