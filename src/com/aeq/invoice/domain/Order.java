package com.aeq.invoice.domain;

import java.util.Map;

public class Order {

    private Long id;
    private Long customerid;
    private Map<Long, Integer> products;

    public Order() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCustomerid() {
        return customerid;
    }

    public void setCustomerid(Long customerid) {
        this.customerid = customerid;
    }

    public Map<Long, Integer> getProducts() {
        return products;
    }

    public void setProducts(Map<Long, Integer> products) {
        this.products = products;
    }

    @Override
    public String toString() {
        return "Order{" +
                "id=" + id +
                ", customerid=" + customerid +
                ", products=" + products +
                '}';
    }
}
