package com.aeq.invoice.domain;

import java.util.Date;

public class Invoice {

    private Long id;
    private Long orderId;
    private Long custome;
    private Date date;
    private String invoiceNumber;
    private Double totalPrice;

    public Invoice(Long id, Long orderId, Date date, String invoiceNumber) {
        this.id = id;
        this.orderId = orderId;
        this.date = date;
        this.invoiceNumber = invoiceNumber;
    }

    public Invoice() {
    }

    public Long getCustome() {
        return custome;
    }

    public void setCustome(Long custome) {
        this.custome = custome;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(Double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    public void setInvoiceNumber(String invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }


    @Override
    public String toString() {
        return "Invoice{" +
                "id=" + id +
                ", orderId=" + orderId +
                ", custome=" + custome +
                ", date=" + date +
                ", invoiceNumber='" + invoiceNumber + '\'' +
                ", totalPrice=" + totalPrice +
                '}';
    }
}
