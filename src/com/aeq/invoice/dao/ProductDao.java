package com.aeq.invoice.dao;

import com.aeq.invoice.domain.Product;

import java.util.List;
import java.util.Optional;

public interface ProductDao {

    boolean add(Product product);

    Optional<Product> getById(Long id);

    List<Product> list();

    boolean updateProduct(ProductDao productDao, Product product);

}
