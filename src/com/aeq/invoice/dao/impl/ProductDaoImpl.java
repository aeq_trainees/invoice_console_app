package com.aeq.invoice.dao.impl;

import com.aeq.invoice.dao.ProductDao;
import com.aeq.invoice.domain.Product;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class ProductDaoImpl implements ProductDao {

    List<Product> products = new ArrayList<>();

    @Override
    public boolean add(Product product) {
        return products.add(product);
    }

    @Override
    public Optional<Product> getById(Long id) {
        return products.stream().filter(product -> product.getId().equals(id)).findFirst();
    }

    @Override
    public List<Product> list() {
        return products;
    }

    @Override
    public boolean updateProduct(ProductDao productDao, Product product) {
        Product currentProduct = getById(product.getId()).get();
        products.remove(currentProduct);
        products.add(product);
        return false;
    }
}
