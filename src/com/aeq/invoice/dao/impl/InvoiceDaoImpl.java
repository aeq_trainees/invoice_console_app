package com.aeq.invoice.dao.impl;

import com.aeq.invoice.dao.InvoiceDao;
import com.aeq.invoice.domain.Invoice;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class InvoiceDaoImpl implements InvoiceDao {

    List<Invoice> invoices = new ArrayList<>();

    @Override
    public boolean add(Invoice invoice) {
        return invoices.add(invoice);
    }

    @Override
    public Optional<Invoice> getById(Long id) {
        return invoices.stream().filter(invoice -> invoice.getId().equals(id)).findFirst();
    }

    @Override
    public List<Invoice> list() {
        return invoices;
    }
}
