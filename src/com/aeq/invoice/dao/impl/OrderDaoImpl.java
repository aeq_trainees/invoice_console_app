package com.aeq.invoice.dao.impl;

import com.aeq.invoice.dao.OrderDao;
import com.aeq.invoice.domain.Order;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class OrderDaoImpl implements OrderDao {

    List<Order> orders = new ArrayList<>();

    @Override
    public boolean add(Order order) {

        return orders.add(order);
    }

    @Override
    public Optional<Order> getById(Long id) {
        return orders.stream().filter(order -> order.getId().equals(id)).findFirst();
    }

    @Override
    public List<Order> list() {
        return orders;
    }
}
