package com.aeq.invoice.dao.impl;

import com.aeq.invoice.dao.CustomerDao;
import com.aeq.invoice.domain.Customer;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class CustomerDaoImpl implements CustomerDao {

    List<Customer> customers = new ArrayList<>();

    @Override
    public boolean add(Customer customer) {
        return customers.add(customer);
    }

    @Override
    public Optional<Customer> getById(Long id) {
        return customers.stream().filter(customer -> customer.getId().equals(id)).findFirst();
    }

    @Override
    public List<Customer> list() {
        return customers;
    }
}
