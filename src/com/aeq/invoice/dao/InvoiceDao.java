package com.aeq.invoice.dao;

import com.aeq.invoice.domain.Invoice;

import java.util.List;
import java.util.Optional;

public interface InvoiceDao {

    boolean add(Invoice invoice);

    Optional<Invoice> getById(Long id);

    List<Invoice> list();
}
