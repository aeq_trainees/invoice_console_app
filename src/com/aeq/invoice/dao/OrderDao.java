package com.aeq.invoice.dao;

import com.aeq.invoice.domain.Order;

import java.util.List;
import java.util.Optional;

public interface OrderDao {

    boolean add(Order order);

    Optional<Order> getById(Long id);

    List<Order> list();
}
