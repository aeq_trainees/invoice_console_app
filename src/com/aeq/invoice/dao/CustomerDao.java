package com.aeq.invoice.dao;

import com.aeq.invoice.domain.Customer;

import java.util.List;
import java.util.Optional;

public interface CustomerDao {

    boolean add(Customer customer);

    Optional<Customer> getById(Long id);

    List<Customer> list();
}
