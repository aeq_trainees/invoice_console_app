package com.aeq.invoice.service;

import com.aeq.invoice.dao.ProductDao;
import com.aeq.invoice.domain.Product;

import java.util.Optional;

public interface ProductService {

    boolean addProduct(ProductDao productDao);

    Optional<Product> findProductById(ProductDao productDao, Long id);
}
