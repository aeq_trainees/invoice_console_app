package com.aeq.invoice.service;

import com.aeq.invoice.dao.CustomerDao;
import com.aeq.invoice.dao.InvoiceDao;
import com.aeq.invoice.dao.OrderDao;
import com.aeq.invoice.dao.ProductDao;
import com.aeq.invoice.domain.Order;

import java.util.Optional;

public interface OrderService {

    boolean addOrder(CustomerDao customerDao, OrderDao orderDao, InvoiceDao invoiceDao, ProductDao productDao);

    Optional<Order> findByOrderId(OrderDao orderDao, Long id);
}
