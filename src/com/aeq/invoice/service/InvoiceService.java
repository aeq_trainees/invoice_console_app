package com.aeq.invoice.service;

import com.aeq.invoice.dao.CustomerDao;
import com.aeq.invoice.dao.InvoiceDao;
import com.aeq.invoice.dao.OrderDao;
import com.aeq.invoice.dao.ProductDao;
import com.aeq.invoice.domain.Invoice;
import com.aeq.invoice.domain.Product;

import java.util.List;
import java.util.Optional;

public interface InvoiceService {

    boolean addInvoice(CustomerDao customerDao, OrderDao orderDao, InvoiceDao invoiceDao, ProductDao productDao, Long customerId, Long orderId);

    Invoice findInvoiceByInvoiceNumber(InvoiceDao invoiceDao, String invoiceNumber);

    Optional<Invoice> findByOrderId(OrderDao orderDao, InvoiceDao invoiceDao, Long orderId);

    List<Invoice> findByCustomerId(OrderDao orderDao, InvoiceDao invoiceDao, Long orderId);

}
