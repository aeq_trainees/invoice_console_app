package com.aeq.invoice.service;

import com.aeq.invoice.dao.CustomerDao;
import com.aeq.invoice.domain.Customer;

import java.util.Optional;

public interface CustomerService {

    boolean addCustomer(CustomerDao customerDao);

    Optional<Customer> findCustomerByid(CustomerDao customerDao, Long id);
}
