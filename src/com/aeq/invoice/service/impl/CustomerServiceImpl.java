package com.aeq.invoice.service.impl;

import com.aeq.invoice.dao.CustomerDao;
import com.aeq.invoice.domain.Customer;
import com.aeq.invoice.service.CustomerService;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Optional;

public class CustomerServiceImpl implements CustomerService {
    @Override
    public boolean addCustomer(CustomerDao customerDao) {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        Customer customer = new Customer();

        try {
            System.out.println("Enter Customer Id:(0-1)");
            customer.setId(Long.valueOf(br.readLine()));
            System.out.println("Enter Name:(a-b)");
            customer.setName(br.readLine());
            System.out.println("Enter Customer Address");
            customer.setAddress(br.readLine());

            if (customerDao.getById(customer.getId()).isPresent()) {
                System.out.println("Custer already exists by given id");
                return false;
            } else {
                return customerDao.add(customer);
            }
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Enter Valid input");
            return false;
        }

    }

    @Override
    public Optional<Customer> findCustomerByid(CustomerDao customerDao, Long id) {
        return customerDao.getById(id);
    }
}
