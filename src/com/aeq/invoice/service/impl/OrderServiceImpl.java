package com.aeq.invoice.service.impl;

import com.aeq.invoice.dao.CustomerDao;
import com.aeq.invoice.dao.InvoiceDao;
import com.aeq.invoice.dao.OrderDao;
import com.aeq.invoice.dao.ProductDao;
import com.aeq.invoice.domain.Order;
import com.aeq.invoice.domain.Product;
import com.aeq.invoice.service.OrderService;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class OrderServiceImpl implements OrderService {
    @Override
    public boolean addOrder(CustomerDao customerDao, OrderDao orderDao, InvoiceDao invoiceDao, ProductDao productDao) {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        boolean status = true;

        Order order = new Order();
        System.out.println("Enter order id");
        try {
            order.setId(Long.valueOf(br.readLine()));
        } catch (IOException e) {
            System.out.println("enter valid input");
            return false;
        }


        Map<Long, Integer> products = new HashMap<>();
//        List<Long> products = new ArrayList<>();
//        List<Integer> quantities = new ArrayList<>();

        System.out.println("Enter Customer id");

        Long customerId;
        try {
            customerId = Long.valueOf(br.readLine());
            order.setCustomerid(customerId);


            if (customerDao.getById(customerId).isPresent()) {
                while (status) {
                    System.out.println("0. Exit");
                    System.out.println("Select below Products id to add to Order");
                    System.out.println(productDao.list());

                    try {
                        Long pId = Long.valueOf(br.readLine());

                        if (pId != 0) {
                            System.out.println("Enter Quanitity");
                            Integer quanity = Integer.valueOf(br.readLine());

                            Product product = productDao.getById(pId).get();

                            if (product.getAvailableQuantity() >= quanity) {
                                products.put(pId, quanity);
                                product.setAvailableQuantity(product.getAvailableQuantity() - products.get(pId));
                                productDao.updateProduct(productDao, product);
                            } else {
                                System.out.println("Insuffient quantity");
                            }


                        } else {
                            status = false;
                        }

                    } catch (IOException e) {
                        System.out.println("Enter valid input");
                    }

                }


                order.setProducts(products);
                return orderDao.add(order);
            } else {
                System.out.println("invalid customer");
                return false;

            }
        } catch (IOException e) {
            System.out.println("enter valid input");
            return false;
        }


    }

    @Override
    public Optional<Order> findByOrderId(OrderDao orderDao, Long id) {
        return orderDao.getById(id);
    }
}
