package com.aeq.invoice.service.impl;

import com.aeq.invoice.dao.CustomerDao;
import com.aeq.invoice.dao.InvoiceDao;
import com.aeq.invoice.dao.OrderDao;
import com.aeq.invoice.dao.ProductDao;
import com.aeq.invoice.domain.Invoice;
import com.aeq.invoice.domain.Order;
import com.aeq.invoice.domain.Product;
import com.aeq.invoice.service.InvoiceService;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

public class InvoiceServiceImpl implements InvoiceService {


    @Override
    public boolean addInvoice(CustomerDao customerDao, OrderDao orderDao, InvoiceDao invoiceDao, ProductDao productDao, Long customerId, Long orderId) {

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        System.out.println("Enter order id to generate Invoice:");
        try {
            Optional<Invoice> invoiceOptional = findByOrderId(orderDao, invoiceDao, orderId);

            if (invoiceOptional.isPresent()) {
                System.out.println("This order has been added to invoice already");
            } else {
                Invoice invoice = new Invoice();
                invoice.setCustome(customerId);
                invoice.setDate(new Date());
                invoice.setOrderId(orderId);
                System.out.println("Enter Invoice id");
                invoice.setId(Long.valueOf(br.readLine()));
                invoice.setInvoiceNumber("INV00" + invoice.getId());

                Optional<Order> order = orderDao.getById(orderId);
                //sum of products price * quan
                Map<Long, Integer> products = order.get().getProducts();

                Double totalPrice = 0.0;
                for (Long pId : products.keySet()) {
                    Product product = productDao.getById(pId).get();
                    totalPrice += product.getPrice() * products.get(pId);
                }
                invoice.setTotalPrice(totalPrice);

                invoiceDao.add(invoice);
            }
        } catch (Exception e) {
            System.err.println("Enter valid input");
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public Invoice findInvoiceByInvoiceNumber(InvoiceDao invoiceDao, String invoiceNumber) {
        return null;
    }

    @Override
    public Optional<Invoice> findByOrderId(OrderDao orderDao, InvoiceDao invoiceDao, Long orderId) {

        Optional<Invoice> invoice = invoiceDao.list().stream().filter(inv -> inv.getOrderId().equals(orderId)).findAny();

        return invoice;
    }

    @Override
    public List<Invoice> findByCustomerId(OrderDao orderDao, InvoiceDao invoiceDao, Long customerId) {
        return invoiceDao.list().stream().filter(inv -> inv.getCustome().equals(customerId)).collect(Collectors.toList());
    }


}
