package com.aeq.invoice.service.impl;

import com.aeq.invoice.dao.ProductDao;
import com.aeq.invoice.domain.Product;
import com.aeq.invoice.service.ProductService;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Optional;

public class ProductServiceImpl implements ProductService {
    @Override
    public boolean addProduct(ProductDao productDao) {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        Product product = new Product();

        try {
            System.out.println("Enter Id");
            product.setId(Long.valueOf(br.readLine()));
            System.out.println("Enter Name");
            product.setName(br.readLine());
            System.out.println("Enter price ");
            product.setPrice(Double.valueOf(br.readLine()));
            System.out.println("Enter available quantity");
            product.setAvailableQuantity(Integer.valueOf(br.readLine()));

            return productDao.add(product);
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public Optional<Product> findProductById(ProductDao productDao, Long id) {
        return productDao.getById(id);
    }
}
